import { InjectionToken } from '@angular/core';

export let APP_CONFIG = new InjectionToken('app.config');

export const AppConfig: any = {
  app_name: 'ACCOWALL',
  token_name: 'ACCO_WALL_APP',
  protocol: 'http://',
  host: 'demo0713636.mockable.io/',
  api_version: 'v1/'
};
