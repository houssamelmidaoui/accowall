import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { ApiService } from './../providers/api/api.service';
import { ToastService } from './../providers/toast/toast.service';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {

  form: FormGroup;
  submitted: boolean;
  codePattern = '^[1-9][0-9]*$';

  constructor(
    private fb: FormBuilder,
    private api: ApiService,
    private toast: ToastService
  ) { }

  ngOnInit() {
    this.initForm();
  }

  initForm(): void {
    this.form = this.fb.group({
      firstname: [null, Validators.required],
      lastname: [null, Validators.required],
      code: [null, [Validators.required, Validators.pattern(this.codePattern)]],
      password: [null, [Validators.required, Validators.minLength(6)]],
      confirmPassword: [null, [Validators.required, this.validPasswordConfirmation]]
    });
  }

  get f() { return this.form.controls; }

  isFieldInvalid(field: string) {
    return (
      (!this.form.get(field).valid && this.form.get(field).touched) ||
      (this.form.get(field).untouched && this.submitted)
    );
  }

  validPasswordConfirmation(c: FormControl) {
    return c.root.get('password') && c.root.get('password').value === c.value ? null : {
      validPasswordConfirmation: true
    };

  }

  onSubmit() {
    if (this.form.valid) {
      let that = this;
      this.api.register(this.form.value).subscribe(
        res => {
          that.toast.success(res['msg']);
        },
        err => {
          that.toast.error(err.error['msg']);
        }
      );
    }
    this.submitted = true;
  }

}
