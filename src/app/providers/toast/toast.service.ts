import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

@Injectable({
  providedIn: 'root'
})
export class ToastService {

  timeout: 3000;

  constructor(
    private toastr: ToastrService
  ) { }

  success(msg): void {
    this.toastr.success('', msg, {
      timeOut: this.timeout,
      closeButton: true
    });
  }

  error(msg): void {
    this.toastr.error('', msg, {
      timeOut: this.timeout,
      closeButton: true
    });
  }
}
